#!/bin/sh

ips=""

for ip in $(curl https://www.cloudflare.com/ips-v4); do
    ips="$ip,$ips"
done

# remove the last comma
ips=${ips%?}

gcloud compute firewall-rules create cloudflare-whitelist \
    --action allow \
    --source-ranges $ips \
    --rules tcp:443,tcp:80
gcloud compute firewall-rules create ignore-all-ingress \
    --action deny \
    --priority 65535 \
    --source-ranges 0.0.0.0/0 \
    --rules all
